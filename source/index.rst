.. Generator dokumentacji Sphinx - zadanie documentation master file, created by
   sphinx-quickstart on Mon Nov  6 15:51:48 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Odsylacze
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Generator Dokumentacji Sphinx
====================================

.. image:: sphinx.png
   :width: 400
   :alt: Generator Dokumentacji Sphinx

Sphinx to system tworzenia i generowania dokumentacji technicznej, który konwertuje pliki źródłowe reStructuredText najczęściej do dokumentu HTML, ale możliwe jest również określenie innych formatów wyjściowych, takich jak LaTeX, PDF, ePUB, man.

Sphinx został opracowany specjalnie na potrzeby tworzenia dokumentacji dla języka programowania Python i jest używany do tworzenia dokumentacji dla wielu ważnych projektów Pythona, takich jak Django, NumPy, SciPy, Scikit-Learn, Matplotlib i wielu innych.



Funkcje Sphinx'a
=====================================

Sphinx ma szereg funkcji, które ułatwiają tworzenie dokumentacji.

Jedną z nich jest możliwość tworzenia dokumentacji dla różnych typów obiektów Pythona, takich jak klasy, metody, funkcje itp., za pomocą specjalnych dyrektyw.

Sphinx posiada również mechanizm automatycznego generowania dokumentacji na podstawie komentarzy w kodzie źródłowym, co znacznie przyspiesza proces tworzenia dokumentacji.

Dodatkowo, Sphinx umożliwia tworzenie pełnotekstowych indeksów, co ułatwia przeszukiwanie dokumentacji.

- Formaty wyjściowe: HTML (w tym pomoc HTML dla systemu Windows), LaTeX (do drukowalnych wersji PDF), ePub, Texinfo, strony podręcznika, tekst zwykły.

- Obszerne odnośniki: semantyczne znaczniki i automatyczne linki do funkcji, klas, cytowań, terminów słownika i podobnych informacji.

- Struktura hierarchiczna: łatwe definiowanie drzewa dokumentów, z automatycznymi linkami do rodzeństwa, rodziców i dzieci.

- Automatyczne indeksy: indeks ogólny, jak również indeksy modułów specyficznych dla języka.

- Obsługa kodu: automatyczne podświetlanie przy użyciu podświetlacza Pygments.

- Rozszerzenia: automatyczne testowanie fragmentów kodu, dołączanie docstrings z modułów Pythona (dokumentacja API) za pomocą wbudowanych rozszerzeń, a także wiele innych funkcji za pośrednictwem rozszerzeń innych firm.

- Motywy: modyfikacja wyglądu i uczucia wyjść poprzez tworzenie motywów, i ponowne użycie wielu motywów innych firm.

- Rozszerzenia użytkowników: dziesiątki rozszerzeń przekazanych przez użytkowników; większość z nich można zainstalować z PyPI.



Instalacja Sphinx'a
=====================================

Z uzyciem menadzera pakietow `pip`:

.. code-block:: bash

  $ pip install -U sphinx


=====
Linux
=====

- Debian/Ubuntu

Zainstaluj pakiet python3-sphinx z uzyciem `apt-get`:

.. code-block:: bash

  $ apt-get install python3-sphinx

Jezeli nie byl jeszcze zainstalowany, to takze zainstaluje python'a.

- RHEL, CentOS

Zainstaluj pakieg python-sphinx z uzyciem `yum`:

.. code-block:: bash

  $ yum install python-sphinx

Jezeli nie byl jeszcze zainstalowany, to takze zainstaluje python'a.


=====
macOS
=====
Sphinx moze byc zainstalowany z uzyciem `Homebrew` lub jako paczka z dystrybucji pythona takiej jak `Anaconda`.

- Homebrew

.. code-block:: bash

  $ brew install sphinx-doc


=======
Windows
=======
Sphinx moze byc zainstalowany z uzyciem `Chocolatey` lub python pip.

- Chocolatey

.. code-block:: bash

  $ choco install sphinx

Powinienes miec zainstalowanego `Chocolatey` przed uruchamianiem tej komendy.



Link do Github'a Sphinx'a
=========================

https://github.com/sphinx-doc/sphinx

.. image:: github.png
   :width: 800
   :alt: Github Sphinx'a

